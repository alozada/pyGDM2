.. raw:: html

    <video autoplay loop poster="_static/overview_static.png">
        <source src="_static/overview.mp4" type="video/mp4">
        <source src="_static/overview.webm" type="video/webm">
        Sorry, your browser doesn't support HTML5 video.
    </video>



Overview
**************************

Here we give a short survey of what pyGDM does and what it is capable of.


Simulation goal
=========================================

pyGDM is based on the Green Dyadic Method (GDM) which resolves an optical Lippmann-Schwinger equation and hence **calculates the total field *inside* a nanostructure**, embedded in a constant environment.
GDM is a frequency-domain method, hence the fields are required to be monochromatic and time-harmonic.

From the complex electromagnetic fields inside the nanostructure, further physical quantities can be derived such as local fields outside the nanostructure, far-field scattering and extinction cross-sections or radiation patterns and local heat generation. It is also possible to calculate local densities of photonic states and hence the decay rates of quantum emitters close to the structure.




Volume discretization
=========================================

The GDM is a volume integral equation approach.
The volume integration runs over the nanostructure. It is numerically implemented as a **discretization of the structure volume on a regular mesh**. 
In pyGDM, the discretization is done either on a cubic grid (as shown in the image below), or on a hexagonal compact grid.

.. figure:: _static/3D_volume_discretization_discretized.jpg
..    :align: left




Simulation frame
=========================================

An advantage of GDM over domain discretization techniques such as FDTD is that only the nanostructure is discretized. The environment in which the nanostructure is embedded is described by the Green's dyadic function, used in the calculation. 
As shown in the below figure, **pyGDM implements a Green's dyad for a 3-layer environmental frame, the nanostructure itself lying in the center layer**.
The used Green's dyad is based on a non-retarded approximation using mirror-charges. In consequence, for example near-field effects on metallic substrates won't be correctly reflected in pyGDM.

.. figure:: _static/sketch_reference_system.png
..    :align: left

The below code example shows how to place a nano-sphere in vacuum by setting n1=n2=n3.
By default, nanostructures are generated such, that they are lying on the interface between substrate and environment. If not specified in `structures.struct`, the spacing has a value of 5000nm. 
*Note* that the nanostructures **must** be fully inside the environment layer "n2".


.. code-block:: python

    from pyGDM2 import structures
    from pyGDM2 import materials
    
    ## discretization grid stepsize
    step = 20
    ## structure is defined as list of coordinates. Here: sphere of 120nm radius, cubic discretization
    geometry = structures.sphere(step, R=6, mesh='cube')
    ## material of nanostructure: constant refractive index
    material = materials.dummy(2.0)

    ## place the sphere in vacuum (--> no substrate: n1=n2, no cladding: n3=n2)
    n1 = 1.0
    n2 = 1.0
    n3 = 1.0   # n3 is optional, by default it is set to the value of n2

    ## all information is collected in a "structure" object
    struct = structures.struct(step, geometry, material, n1,n2, 
               n3=n3, normalization=structures.get_normalization(mesh='cube'),
               spacing=5000)
